# Welcome to my Portfolio!👋

[![LinkedIn](https://img.shields.io/badge/-LinkedIn-blue.svg?style=flat&logo=linkedin)](https://linkedin.com/in/joshuabertucci)
[![Website](https://img.shields.io/static/v1?label=website&url=https://joshuabertucci.com&message=joshuabertucci.com&color=blueviolet)](https://joshuabertucci.com)

Welcome to my GitLab portfolio! I am an IT graduate with a huge passion for software engineering, and more specifically, web development and devops. My goal is to use my skills and knowledge to create smart, innovative solutions that make a positive influence on other people's lives.

In this portfolio, you'll find a selection of projects that demonstrate my skills and experience as a software engineer. In each project, you'll find a detailed summary of the project, how to operate the project and what technology stack I used. Please note, this is still a work in progress.

Feel free to browse through my portfolio, and if you have any questions or comments , don't hesitate to reach out to me through my email: [mail@joshuabertucci.com](mailto:mail@joshuabertucci.com).



Thank you for visiting!
